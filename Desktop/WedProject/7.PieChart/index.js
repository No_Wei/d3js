// javascript
var data = [
  //data will overlap if percent is over 100
    {"majors": "Eco", "percentage": 16},
    {"majors": "comp sci", "percentage": 22},
    {"majors": "pharmacy", "percentage": 26},
    {"majors": "Law", "percentage": 15},
    {"majors": "Biology", "percentage": 13},
    {"majors": "Language", "percentage": 8}
];
//radius take in the lowest value for svgWidth and svgHeight and divide by 2
//lowest number is 300, that way the circle can remain inside the box
var svgWidth = 500, svgHeight = 300, radius =  Math.min(svgWidth, svgHeight) / 2;
var svg = d3.select('svg')
    .attr("width", svgWidth)
    .attr("height", svgHeight);

//Taking the group and shifting it
var g = svg.append("g")
    .attr("transform", "translate(" + 300 + "," + 150 + ")") ;
    //transform, translate(300,150

    //assigning color to the scale
    var color = d3.scaleOrdinal(d3.schemeCategory20);

var path = d3.arc()
    .outerRadius(radius)
    .innerRadius(10); //inner radius is how big we want the center to be

    //d3.pie allows the vlaues to become compatiable to the pie chart
    var pie = d3.pie().value(function(d) {
         return d.percentage; //assins values of the data stored in percentage
    });

var arc = g.selectAll("arc")
    .data(pie(data))
    .enter()
    .append("g");

//calls for the percentage and assigning it a color
arc.append("path")
    .attr("d", path) //takes the data of the path and fills with color
    .attr("fill", function(d) { return color(d.data.percentage); });

var label = d3.arc()  //positioning the labels with radius
    .outerRadius(radius)  //requires a inner and outer radius
    .innerRadius(0);

arc.append("text")
    .attr("transform", function(d) {
        return "translate(" + label.centroid(d) + ")"; //places label in the center of each sector
    })
    .attr("text-anchor", "middle") //text-anchor allows a text and middle just centers the text
    .text(function(d) { return d.data.majors+":"+d.data.percentage+"%"; }); //returns the output into the centroid
