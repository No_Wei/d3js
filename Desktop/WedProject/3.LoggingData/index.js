// javascript
var dataset = [1, 2, 3, 4, 5];

d3.select('body')
    .selectAll('p') //sellect all paragraph tags
    .data(dataset)  //we pass in our data set
    .enter()        //method will take data items one by one
    .append('p')    // appends paragraph for each data element
  //  .text('D3 is awesome!!');
    .text(function(d) { return d; }); //function takes our data and turns it
