// javascript

var dataset = [1,2,3,4,5,6];
var svgWidth = 500, svgHeight = 300, barPadding = 5;
var barWidth = (svgWidth / dataset.length); //taking the svgWidth(500) and dividing it by the amount of data we have, that way bar remains even

var svg = d3.select('svg')
   .attr("width", svgWidth)
   .attr("height", svgHeight);

var yScale = d3.scaleLinear()   //apply scaleing, takes the max number from the data set
    .domain([0, d3.max(dataset)])
    .range([0, svgHeight]);

var barChart = svg.selectAll("rect")
   .data(dataset)
   .enter()
   .append("rect")
   .attr("y", function(d) {
        return svgHeight - yScale(d) //need to subtract according to the auto adjusted scale, this creates the scale
   })
   .attr("height", function(d) {     //after scale is created, take the height and outputs the scale
       return yScale(d);
   })
   .attr("width", barWidth - barPadding) //takes the length of width and subtracts, to leave space
   .attr("transform", function (d, i) {//takes function d and assigns it +1
       var translate = [barWidth * i, 0];  //shifting the bar over
       return "translate("+ translate +")";//this will return the cordinates of where to transform
   });

var text = svg.selectAll("text")
   .data(dataset)  //take the data set
   .enter()        //enter it one at a time
   .append("text") //appending it as text
   .text(function(d) {
       return d;     //this returns the numbers
   })
   .attr("y", function(d, i) {
       return svgHeight - yScale(d); //takes the heigh subtract by data will place digits on the top
   })
   .attr("x", function(d, i) {
       return barWidth * i; //centers the numbers 10 pix
   })
   .attr("fill", "red");
