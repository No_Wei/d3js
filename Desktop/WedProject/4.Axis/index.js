      // D3 gives us an api to allow us to create axises
      d3.axisTop()
      d3.axisRight()
      d3.axisBottom()
      d3.axisLeft()

      var data= [80, 100, 56, 120, 180, 30, 40, 120, 160];
      var data1= [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
      var Width = 500, Height = 300;

      var svg = d3.select('svg') //applying the width and high of the svg box
         .attr("width", Width)
         .attr("height", Height);


      var xScale = d3.scaleLinear()
         .domain([0, d3.max(data)]) //takes in integers between 0 to max integer
         .range([0, 400]); //represents the length of the x-axis

      var yScale = d3.scaleLinear()
         .domain([0, d3.max(data1)]) //this the range of the data from 0 to max
         .range([Height, 0]);   //represents the length of the x-axis

      var x_axis = d3.axisBottom().scale(xScale); //call for x-axis

      var y_axis = d3.axisLeft().scale(yScale);   //call for y-axis

      svg.append("g")                             //group element
         .attr("transform", "translate(50, 0)")  //takes the y-axis and transforms it(shift it)
         .call(y_axis);

      var xAxisTranslate = Height - 20;

      svg.append("g")
         .attr("transform", "translate(50, " + xAxisTranslate  +")")
         .call(x_axis);
