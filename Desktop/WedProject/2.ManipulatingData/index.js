// javascript
d3.select();    // returns the first accepted tag that matches the criteria
d3.selectAll(); // returns all the elements matching the criteria

//takes the first h1 and changes the style to red
d3.select('h1').style('color', 'red')

.text('Updated h1 tag'); //overrides the string in the h1 tag and updates it

//insteal of in html, d3 allows you to create a body and append a paragraph
d3.select('body').append('p').text('First Paragraph');
d3.select('body').append('p').text('Second Paragraph');
d3.select('body').append('p').text('Third Paragraph');

//this will assign all p with color blue.
d3.selectAll('p').style('color',"blue");
