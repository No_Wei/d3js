'use strict';

//EXAMPLE ONE
var data = [
  {major: 'Eco', percentage: 16},
  {major: 'comp sci', percentage: 22},
  {major: 'pharmacy', percentage: 26},
  {major: 'Law', percentage: 15},
  {major: 'Biology', percentage: 13},
  {major: 'Language', percentage: 8},
];

//pie is built into d3
var pie = d3.layout.pie()
  .value(function(d) { return d.percentage }) //this percentages the array

//now connect the data with each individual slice
var slices = pie(data);

//built in feature that sets the size of the pie chart
var arc = d3.svg.arc()
  .innerRadius(0)
  .outerRadius(100);

  /*d3 uses this helper called category10 which provies you with 10 different colors
    and it will match the color of your data to the different sectors of the pie chart
  */
var color = d3.scale.category10(); //this is the class pie which will be called in the html file giving it the width and svgHeight
var svg = d3.select('svg.pie'); //groups different parts
var g = svg.append('g')
  .attr('transform', 'translate(300, 100)')

g.selectAll('path.slice') //sellect all the slices in the data and pass through the data with enter
  .data(slices)
    .enter()              //all the information now will be entered into the document one at a time
      .append('path')     //append to the path
        .attr('class', 'slice')
        .attr('d', arc)   //creates the pie arc
        .attr('fill', function(d){  //fill color will be determined by passing in the data
          return color(d.data.major);
        });

// building a table
svg.append('g')
  .attr('class', 'table')  .attr('transform', 'translate(0, 25)')
    .selectAll('text')
    .data(slices)
      .enter()
        .append('text')
          .text(function(d) { return '• ' + d.data.major + ' (' + d.data.percentage + '%)'; })
          .attr('fill', function(d) { return color(d.data.major); })
          .attr('y', function(d, i) { return 20 * (i + 1); })
