
//this sets up the size of the chart
var margin = {top:20, right:20, bottom: 20, left:50},
  width = 600 - margin.left - margin.right,
  height = 400 - margin.top - margin.bottom;

//parse data from file
//this is a built in function which calls to take the data and parse it as the format given
var parseDate = d3.time.format("%d-%b-%y").parse;

//scales

//time function will match with the time in our dataset and place it in the x axis
//.time takes a look at the date data and determines what it means
var x =d3.time.scale()
  .range([0,width]);

//scale function will call for the number of users and place it in the y axis
var y = d3.scale.linear()
  .range([height, 0]);

//set x axis
var xAxis = d3.svg.axis()
  .scale(x)  //scale calls for x
  .orient("bottom"); //orientation will be on the bottom

//set y axis
var yAxis = d3.svg.axis()
    .scale(y) //scale calls for y
    .orient('left') //orients it on the left side

var line = d3.svg.line() //this is a new svg line from d3
  .x(function(d) { return x(d.date); })  //this will automatically draw the line base on the x cordinate using the date data
  .y(function(d) { return y(d.users); }) //this will automatically draw the line base on the y cordinate using the users data

var svg = d3.select(".linechart").append("svg") //take the class linechart and append the svgWidth
  .attr("width", width + margin.left + margin.right)
  .attr("width", width + margin.left + margin.bottom)
  .append("g")  //group the chart and shift it base on the margin provided
  .attr("transform", "translate(" + margin.left + ", " + margin.top +")");

  //import data
  d3.tsv("data.tsv", function(error, data) { //accepts a tsv file and if there is error, throw error
    if (error) throw error;
  })

//traverse the data
data.forEach(function(d) {  //for each thing in the data
  d.date = parseDate(d.date);
  d.users = +d.users;
});

//destablish domain for x and y axis
//extent helps find a minimum and maximum values of what is ask in the data
x.domain(d3.extent(data, function(d) { return d.data; }));
y.domain(d3.extent(data, function(d) { return d.users; }));

//add groups

svg.append("g")
  .attr("class", "x axis")
  .attr("transform", "translate" (0, " + height + "))
  .call(xAxis);

svg.append("g")
  .attr("class" ,"y axis")
  .call(yAxis)
  .append("text")
  .attr("transform", "rotate(-90)")
  .attr("y", 6)
  .attr("dy", ".71em")
  .style("text-anchor", "end")
  .text("Users (unique)");

svg.append("path")
  .datum(data)
  .attr("class", "line")
  .attr("d", line)
