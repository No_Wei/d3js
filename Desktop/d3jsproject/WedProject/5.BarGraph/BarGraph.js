// javascript
var dataset = [80, 100, 56, 120, 180, 30, 40, 120, 160];

var svgWidth = 500, svgHeight = 300, barPadding = 5;
var barWidth = svgWidth / dataset.length; //taking the svgWidth(500) and dividing it by the amount of data we have, that way bar remains even

var svg = d3.select("svg") //svg container
   .attr("width", svgWidth)  //assigning the width of container
   .attr("height", svgHeight);//assign the height of container

//positioning of bar
var barChart = svg.selectAll("rect") //sellect all the rectangles
   .data(dataset)                    //call for dataset
   .enter()                          //enter will now take each data and perform the next operation
   .append("rect")         //svgHeight pushes the rect down by 300, buy subtracting the from the data, it pushes the data up
   .attr("y", function(d) {
        return svgHeight - (d)
   })
   .attr("height", function(d) {     //takes the data and assigns the height
       return d;
   })
   .attr("width", barWidth - barPadding) //takes the length of width and subtracts, to leave space
   .attr("transform", function (d, i) {//takes function d and assigns it +1
       var translate = [barWidth * i, 0];  //shifting the bar over
       return "translate("+ translate +")";//this will return the cordinates of where to transform
   });

var text = svg.selectAll("text")
   .data(dataset)  //take the data set
   .enter()        //enter it one at a time
   .append("text") //appending it as text
   .text(function(d) {
       return d;     //this returns the numbers
   })
   .attr("y", function(d, i) {
       return svgHeight - d - 25; //takes the heigh subtract by data will place digits on the top
   })
   .attr("x", function(d, i) {
       return barWidth * i + 10; //centers the numbers 10 pix
   })
   .attr("fill", "red");
