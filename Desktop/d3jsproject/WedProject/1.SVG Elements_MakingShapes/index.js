// javascript

var svgWidth = 600, svgHeight = 500; //provide box
var svg = d3.select("svg")
   .attr("width", svgWidth)
   .attr("height", svgHeight)
   .attr("class", "svg-container")

//line takes the cordinates from x1,y1 and draws a line to y2,y1
var line = svg.append("line")
   .attr("x1", 100)
   .attr("y1", 50)
   .attr("x2", 500)
   .attr("y2", 50)
   .attr("stroke", "blue");

var rect = svg.append("rect")
   .attr("x", 100)  
   .attr("y", 100)
   .attr("width", 200)
   .attr("height", 100)
   .attr("fill", "#9B95FF");


//circle takes in the x and y values from the center using cx and cy
var circle = svg.append("circle")
   .attr("cx", 200)
   .attr("cy", 300)
   .attr("r", 80)   //radius
   .attr("fill", "#7CE8D5");  //fill use to color
